someAsyncMethod().then(function(result) {
  DoSomethingSynchronous();
  return someOtherAsyncMethod("stuff");
}).then(function(result) {
  return yetAnotherAsyncMethod("things", "and stuff");
}).then(function(result) {
  console.log("Whoo, we're done!");
  return exit(0);
});
